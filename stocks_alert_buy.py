#!/usr/bin/python3
# -*- coding: utf-8 -*-

from datetime import date, timedelta
import datetime
import requests
import json
import time
import os
import sys

__author__ = "Fagnera"
__email__  = "fagner.chaves.silva@gmail.com"

""" script para coletar preços das ações e
    enviar alerta caso valor esteja abaixo do valor de vpa 
"""

now = datetime.datetime.now()
now_dt = now.strftime("%A")

if (( now_dt == "Saturday" ) or ( now_dt == "Sunday" )):
    print ("B3 Closed")
    sys.exit(1)
else:
    print ("B3 Open")

date_today = date.today()
date_yesterday = timedelta(1)
date = str(date_today - date_yesterday)
print (str("Data da Cotação: "+ date +"\n"))

api_key = os.environ.get('API_KEY')
webhook = os.environ.get('WEBHOOK')

try:
    os.environ['API_KEY']
    os.environ['WEBHOOK']
except KeyError:
    print ("Necessario definir variaveis de ambiente")
    sys.exit(1)

headers = {'Content-Type': 'application/json'}

lista = ['ITUB3.SA','ITSA3.SA','HAPV3.SA','ENBR3.SA','BBDC3.SA','BBAS3.SA']

for i in lista:
    response_stock = requests.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+ i +"&interval=5min&apikey="+ api_key)
    response_data  = json.loads(response_stock.content)
    price = float(response_data["Time Series (Daily)"][date]["3. low"])
    print ("PRICE:", price)
    print ("TICKER: "+ i )
    time.sleep(10)

    payload = {
      "username": "Fanera",
      "content": "Cotação" ,
      "text": "stocks_alert",
      "blocks": [
        {
          "type": "section",
          "text": {
              "type": "mrkdwn",
              "text": "Ticker: %s" %i,
          }
        },
        {
          "type": "section",
          "text": {
              "type": "mrkdwn",
              "text": 'Cotação Abaixo: %s ' %price,
            }
        },
      ]
    }

    if ((i == "HAPV3.SA") and float(price < 12.00)):
        print (i, ": Cotação Abaixo do VPA \n")
        response_slack = requests.post(webhook, data=json.dumps(payload), headers=headers)

    elif (( i == "ITSA3.SA" ) and float( price < 11.00 )):
        print (i, ": Cotação Abaixo do VPA \n")
        response_slack = requests.post(webhook, data=json.dumps(payload), headers=headers)

    elif (( i == "ITUB3.SA" ) and  float(price < 21.00)):
        print (i, ": Cotação Abaixo do VPA \n")
        response_slack = requests.post(webhook, data=json.dumps(payload), headers=headers)

    elif (( i == "BBDC3.SA" ) and  float(price < 18.00)):
        print (i, ": Cotação Abaixo do VPA \n")
        response_slack = requests.post(webhook, data=json.dumps(payload), headers=headers)

    elif (( i == "ENBR3.SA" ) and  float(price < 19.00)):
        print (i, ": Cotação Abaixo do VPA \n")
        response_slack = requests.post(webhook, data=json.dumps(payload), headers=headers)

    elif (( i == "BBAS3.SA" ) and  float(price < 25.00) ):
        print (i, ": Cotação Abaixo do VPA \n")
        response_slack = requests.post(webhook, data=json.dumps(payload), headers=headers)
    else:
        print (i, ": Cotação esta Cara \n")


print ("********* FASE 2 Crypto *********\n")

time.sleep(20)

response = requests.get("https://www.alphavantage.co/query?function=CRYPTO_INTRADAY&symbol=ETH&market=USD&interval=5min&apikey=demo")
data_dict = json.loads(response.content)

data_crypto = data_dict["Meta Data"]["3. Digital Currency Name"]
data_price = data_dict["Time Series Crypto (5min)"]["2021-04-27 02:45:00"]["3. low"]

print ("Cripto: " + data_dict["Meta Data"]["3. Digital Currency Name"])
print ("Menor Cotacao: " + data_dict["Time Series Crypto (5min)"]["2021-04-27 02:45:00"]["3. low"])

print ("Crypto: "+ data_crypto +" Cotação:", data_price)

if int(float(data_price)) < 100.000:
    print ("Cripto OK")
else:
    print ("Cripto Cara")

#!/usr/bin/python3
# -*- coding: utf-8 -*-

from datetime import date, timedelta
from os import environ
import datetime
import requests
import json
import time
import os
import sys

__author__ = "Fagnera"
__email__  = "fagner.chaves.silva@gmail.com"

""" script para coletar preços das ações e
    enviar alerta caso cotaçao esteja acima de determinado preço.
    Indicando uma possivel venda / realização.
"""

now = datetime.datetime.now()
now_dt = now.strftime("%A")

if (( now_dt == "Saturday" ) or ( now_dt == "Sunday" )):
    print ("B3 Closed")
    sys.exit(1)
else:
    print ("B3 Open")

date_today = date.today()
date_yesterday = timedelta(4)
date = str(date_today - date_yesterday)
print (str("Data da Cotação: "+ date +"\n"))

api_key = os.environ.get('API_KEY')
webhook = os.environ.get('WEBHOOK')

try:
    os.environ['API_KEY']
    os.environ['WEBHOOK']
except KeyError:
    print (environ["USER"], "favor definir as variaveis de ambiente API_KEY e WEBHOOK !!!")
    sys.exit(1)


api_key = os.environ.get('API_KEY')

webhook = "https://webhook.site/a4e6b38e-d05c-43c9-b0a9-6a4cca125090"

headers = {'Content-Type': 'application/json'}

lista = ['ITUB3.SA','ITSA3.SA','HAPV3.SA','ENBR3.SA','BBDC3.SA','BBAS3.SA','BBSE3.SA']

for i in lista:
    response_stock = requests.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+ i +"&interval=5min&apikey="+ api_key)
    response_data  = json.loads(response_stock.content)
    price = float(response_data["Time Series (Daily)"]["2021-04-22"]["3. low"])
    print ("PRICE:", price)
    print ("TICKER: "+ i )
    time.sleep(0.5)

    payload = {
      "username": "Fanera",
      "content": "Cotação" ,
      "text": "stocks_alert_sell",
      "blocks": [
        {
          "type": "section",
          "text": {
              "type": "mrkdwn",
              "text": "Ticker: %s" %i,
          }
        },
        {
          "type": "section",
          "text": {
              "type": "mrkdwn",
              "text": 'Cotação Valorizada: %s ' %price,
            }
        },
      ]
    }

    if (( i == "HAPV3.SA") and float( price < 19.00 )):
        print (i, ": Cotação Abaixo \n")

    elif (( i == "ITSA3.SA" ) and float( price < 16.00 )):
        print (i, ": Cotação Abaixo \n")

    elif (( i == "ITUB3.SA" ) and  float( price < 32.00 )):
        print (i, ": Cotação Abaixo \n")

    elif (( i == "BBDC3.SA" ) and  float( price < 30.00 )):
        print (i, ": Cotação Abaixo \n")

    elif (( i == "ENBR3.SA" ) and  price < 27.00 ):
        print (i, ": Cotação Abaixo \n")

    elif (( i == "BBAS3.SA" ) and  price < 40.00 ):
        print (i, ": Cotação Abaixo \n")

    elif (( i == "BBSE3.SA" ) and  float( price < 31.00 )):
        print (i, ": Cotação Abaixo \n")

    else:
        print (i, ": Considerar Realização de Lucro \n")
        response_slack = requests.post(webhook, data=json.dumps(payload), headers=headers)

